//=============================================================================================
// Szamitogepes grafika hazi feladat keret. Ervenyes 2016-tol.
// A //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// sorokon beluli reszben celszeru garazdalkodni, mert a tobbit ugyis toroljuk.
// A beadott program csak ebben a fajlban lehet, a fajl 1 byte-os ASCII karaktereket tartalmazhat.
// Tilos:
// - mast "beincludolni", illetve mas konyvtarat hasznalni
// - faljmuveleteket vegezni a printf-et kiv�ve
// - new operatort hivni a lefoglalt adat korrekt felszabaditasa nelkul
// - felesleges programsorokat a beadott programban hagyni
// - felesleges kommenteket a beadott programba irni a forrasmegjelolest kommentjeit kiveve
// ---------------------------------------------------------------------------------------------
// A feladatot ANSI C++ nyelvu forditoprogrammal ellenorizzuk, a Visual Studio-hoz kepesti elteresekrol
// es a leggyakoribb hibakrol (pl. ideiglenes objektumot nem lehet referencia tipusnak ertekul adni)
// a hazibeado portal ad egy osszefoglalot.
// ---------------------------------------------------------------------------------------------
// A feladatmegoldasokban csak olyan OpenGL fuggvenyek hasznalhatok, amelyek az oran a feladatkiadasig elhangzottak 
//
// NYILATKOZAT
// ---------------------------------------------------------------------------------------------
// Nev    : Barta Gergo
// Neptun : ym4tz1
// ---------------------------------------------------------------------------------------------
// ezennel kijelentem, hogy a feladatot magam keszitettem, es ha barmilyen segitseget igenybe vettem vagy
// mas szellemi termeket felhasznaltam, akkor a forrast es az atvett reszt kommentekben egyertelmuen jeloltem.
// A forrasmegjeloles kotelme vonatkozik az eloadas foliakat es a targy oktatoi, illetve a
// grafhazi doktor tanacsait kiveve barmilyen csatornan (szoban, irasban, Interneten, stb.) erkezo minden egyeb
// informaciora (keplet, program, algoritmus, stb.). Kijelentem, hogy a forrasmegjelolessel atvett reszeket is ertem,
// azok helyessegere matematikai bizonyitast tudok adni. Tisztaban vagyok azzal, hogy az atvett reszek nem szamitanak
// a sajat kontribucioba, igy a feladat elfogadasarol a tobbi resz mennyisege es minosege alapjan szuletik dontes.
// Tudomasul veszem, hogy a forrasmegjeloles kotelmenek megsertese eseten a hazifeladatra adhato pontokat
// negativ elojellel szamoljak el es ezzel parhuzamosan eljaras is indul velem szemben.
//=============================================================================================

#define _USE_MATH_DEFINES
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#if defined(__APPLE__)
#include <GLUT/GLUT.h>
#include <OpenGL/gl3.h>
#include <OpenGL/glu.h>
#else
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)
#include <windows.h>
#endif
#include <GL/glew.h>		// must be downloaded 
#include <GL/freeglut.h>	// must be downloaded unless you have an Apple
#endif

#include <iostream>

const unsigned int windowWidth = 600, windowHeight = 600;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Innentol modosithatod...

constexpr float E = 10-6;
constexpr unsigned int MAX_NUMBER_OF_OBJECTS = 50;
constexpr unsigned int MAX_NUMBER_OF_LIGHT_SOURCES = 10;
constexpr unsigned int MAX_NUMBER_OF_RECURSION_DEPTH = 20;

// OpenGL major and minor versions
int majorVersion = 3, minorVersion = 0;

void getErrorInfo(unsigned int handle) {
	int logLen;
	glGetShaderiv(handle, GL_INFO_LOG_LENGTH, &logLen);
	if (logLen > 0) {
		char * log = new char[logLen];
		int written;
		glGetShaderInfoLog(handle, logLen, &written, log);
		printf("Shader log:\n%s", log);
		delete log;
	}
}

// check if shader could be compiled
void checkShader(unsigned int shader, char * message) {
	int OK;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &OK);
	if (!OK) {
		printf("%s!\n", message);
		getErrorInfo(shader);
	}
}

// check if shader could be linked
void checkLinking(unsigned int program) {
	int OK;
	glGetProgramiv(program, GL_LINK_STATUS, &OK);
	if (!OK) {
		printf("Failed to link shader program!\n");
		getErrorInfo(program);
	}
}

// vertex shader in GLSL
const char *vertexSource = R"(
	#version 130
    precision highp float;

	in vec2 vertexPosition;		// variable input from Attrib Array selected by glBindAttribLocation
	out vec2 texcoord;			// output attribute: texture coordinate

	void main() {
		texcoord = (vertexPosition + vec2(1, 1))/2;							// -1,1 to 0,1
		gl_Position = vec4(vertexPosition.x, vertexPosition.y, 0, 1); 		// transform to clipping space
	}
)";

// fragment shader in GLSL
const char *fragmentSource = R"(
	#version 130
    precision highp float;

	uniform sampler2D textureUnit;
	in  vec2 texcoord;			// interpolated texture coordinates
	out vec4 fragmentColor;		// output that goes to the raster memory as told by glBindFragDataLocation

	void main() {
		fragmentColor = texture(textureUnit, texcoord); 
	}
)";

struct vec4 {
	float v[4];

	vec4(float x = 0, float y = 0, float z = 0, float w = 1) {
		v[0] = x; v[1] = y; v[2] = z; v[3] = w;
	}

	vec4 operator*(float f) {
		return vec4(v[0] * f, v[1] * f, v[2] * f, v[3] * f);
	}

	vec4 operator*(vec4 v1) {
		return vec4(v[0] * v1.v[0], v[1] * v1.v[1], v[2] * v1.v[2], v[3] * v1.v[3]);
	}

	vec4 operator/(vec4 vec) {
		return vec4(v[0] / vec.v[0], v[1] / vec.v[1], v[2] / vec.v[2], v[3] / vec.v[3]);
	}

	vec4 operator+(vec4 vec) {
		return vec4(vec.v[0] + v[0], vec.v[1] + v[1], vec.v[2] + v[2]);
	}

	vec4 operator-(vec4 vec) {
		return vec4(v[0] - vec.v[0], v[1] - vec.v[1], v[2] - vec.v[2]);
	}

	float length() {
		return sqrtf(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
	}

	vec4 normalize() {
		return *this * (1.0f / length());
	}

	static float dot(vec4 v1, vec4 v2) {
		return (v1.v[0] * v2.v[0] + v1.v[1] * v2.v[1] + v1.v[2] * v2.v[2]);
	}

	static vec4 cross(vec4 v1, vec4 v2) {
		return vec4(v1.v[1] * v2.v[2] - v1.v[2] * v2.v[1],
					v1.v[2] * v2.v[0] - v1.v[0] * v2.v[2],
					v1.v[0] * v2.v[1] - v1.v[1] * v2.v[0], 0.0f);
	}

	vec4 invert() {
		return vec4(1.0f / v[0], 1.0f / v[1], 1.0f / v[2], 1.0f / v[3]);
	}
};

float sign(float num) {
	return num / (float)fabs(num);
}

vec4 unit_color(1.0f, 1.0f, 1.0f);

struct Light {
    vec4 position;
    vec4 Lout;
    Light(vec4 pos, vec4 L) : position(pos), Lout(L) {}
};

struct Ray {
    vec4 origin, dir;
    float t;

    Ray(vec4 o, vec4 d) : origin(o), dir(d), t(-1) {}
};

class Camera {
    vec4 lookat, right, up, eye;
public:
    Camera(vec4& l, vec4& e, vec4& u, vec4& r) : lookat(l), right(r), up(u), eye(e) {}

    Ray getRay(int x, int y) {
		float a = (float)(2.0f * (float)x / (float)windowWidth) - 1.0f;
		float b = (float)(2.0f * (float)y / (float)windowHeight) - 1.0f;
		vec4 p = lookat + right * a  + up * b;
        Ray ray(eye, (p - eye).normalize());
        return ray;
    }

	vec4 getViewDir(vec4& p) {
		return (eye - p).normalize();
	}
};

typedef enum Surface {
	rough,
	reflective,
	refractive
} Surface;

class Material {
	Surface surface;
	vec4 F0, k, kd, ks, ka;
    float shininess, n;
public:
	Material(Surface s, float n, vec4 k, vec4 ka, vec4 kd, vec4 ks, float shininess)
			: surface(s), n(n), k(k), ka(ka), kd(kd), ks(ks), shininess(shininess) {
		F0.v[0] = ((n - 1.0f) * (n - 1.0f) + k.v[0] * k.v[0]) / ((n + 1.0f) * (n + 1.0f) + k.v[0] * k.v[0]);
		F0.v[1] = ((n - 1.0f) * (n - 1.0f) + k.v[1] * k.v[1]) / ((n + 1.0f) * (n + 1.0f) + k.v[1] * k.v[1]);
		F0.v[2] = ((n - 1.0f) * (n - 1.0f) + k.v[2] * k.v[2]) / ((n + 1.0f) * (n + 1.0f) + k.v[2] * k.v[2]);
		F0.v[3] = ((n - 1.0f) * (n - 1.0f) + k.v[3] * k.v[3]) / ((n + 1.0f) * (n + 1.0f) + k.v[3] * k.v[3]);
    }

	vec4 getKa() const { return ka; }

	Surface getSurface() const { return surface; }

	vec4 shade(vec4& normal, vec4& viewDir, vec4& lightDir, vec4& inRad) {
		vec4 reflRad;
		float cosTheta = vec4::dot(normal, lightDir);
		if(cosTheta < 0) return reflRad;
		reflRad = inRad * kd * cosTheta;	// Lambert
		vec4 halfway = (viewDir + lightDir).normalize();
		float cosDelta = vec4::dot(normal, halfway);
		if(cosDelta < 0 ) return reflRad;
		return reflRad + inRad * ks * pow(cosDelta, shininess); // Phong
	}

	vec4 reflect(vec4& inDir, vec4& normal) {
		return inDir - normal * vec4::dot(normal, inDir) * 2.0f;
	}

	vec4 refract(vec4& V, vec4& N) {
		float cosa = -vec4::dot(V, N);
		float ior = n;
		if(cosa < 0.0f) { N = N * -1.0f; ior = 1.0f / ior; cosa = -cosa; }
		float disc = 1.0f - (1.0f - cosa * cosa) / ior / ior;
		if(disc < 0.0f) return reflect(V, N);
		return V / ior + N * (cosa / ior - sqrt(disc));
	}

	vec4 Fresnel(vec4& inDir, vec4& normal) {
		float cosa = (float)fabs(vec4::dot(normal, inDir));
		return F0 + (unit_color - F0) * pow(1 - cosa, 5);
	}
};

struct Hit {
    float t;
    vec4 position;
    vec4 normal;
    Material *material;
    Hit() { t = -1; }
};

class Intersectable {
protected:
	Material *material;
public:
	Intersectable(Material *m) : material(m) {}
    virtual Hit intersect(Ray ray) = 0;
};

class Sphere : public Intersectable {
    vec4 center;
    float rad;
public:
    Sphere(vec4& o, float r, Material* mat) : center(o), rad(r), Intersectable(mat) { }

	vec4 getNormal(vec4& hit) {
		return (hit - center).normalize();
	}

    Hit intersect(Ray ray) {
		Hit hit;
		float t = 0;
		float a = vec4::dot(ray.dir, ray.dir);
		float b = vec4::dot((ray.origin - center), ray.dir) * 2.0f;
		float c = vec4::dot((ray.origin - center), (ray.origin - center)) - rad * rad;
		float discr = b * b - 4.0f * a * c;
		float den = 2 * a;
		if (discr < E)
			return hit;
		t =  ((- b - sqrtf(discr)) / den);

		if(t > E) {
			hit.t = t;
			hit.material = material;
			hit.position = ray.origin + ray.dir * (t + E);
			hit.normal = getNormal(hit.position);
			return hit;
		}
		t = ((- b + sqrtf(discr)) / den);
		if(t > E) {
			hit.t = t;
			hit.material = material;
			hit.position = ray.origin + ray.dir * (t - E);
			hit.normal = getNormal(hit.position);
			return hit;
		}
		return hit;
    }
};

class Plane : public Intersectable {
	vec4 p, normal;
    float width, height;
public:
	Plane(vec4 p, vec4 n, Material *m, float w = -1, float h = -1)
            : p(p), normal(n), Intersectable(m), width(w), height(h) { }

	Hit intersect(Ray ray) {
		Hit hit;
		/* n * (x - x0) = 0
		 * n * (ro + rd * t - x0) = 0
		 * n * ro + rd * t * n - x0 * n = 0
		 * n * (ro - x0) + rd * t * n = 0
		 * t = -n * (ro - x0) / rd * n
		 */
		float t = vec4::dot((p - ray.origin), normal) / vec4::dot(ray.dir, normal);
		if (t > E) {
			hit.normal = normal;
			hit.position = ray.origin + ray.dir * (t + E);
			hit.material = material;
			hit.t = t;
		}
		return hit;
	}
};

class Triangle : public Intersectable {
	vec4 r1, r2, r3, normal;
public:
	Triangle(vec4 r1, vec4 r2, vec4 r3, Material *m) : r1(r1), r2(r2), r3(r3), Intersectable(m) {
		normal = (vec4::cross((r2 - r1).normalize(), (r3 - r1).normalize())).normalize();
	}

	Hit intersect(Ray ray) {
		Hit hit;
		float t = vec4::dot((r1 - ray.origin), normal) / vec4::dot(ray.dir, normal);
		if(t > E) {
			vec4 p = ray.origin + ray.dir * t;
			if(vec4::dot(vec4::cross((r2 - r1), (p - r1)), normal) > 0.0f &&
			   vec4::dot(vec4::cross((r3 - r2), (p - r2)), normal) > 0.0f &&
			   vec4::dot(vec4::cross((r1 - r3), (p - r3)), normal) > 0.0f) {
				hit.normal = normal * -1.0f;
				hit.t = t;
				hit.material = material;
				hit.position = p;
			}
		}
		return hit;
	}
};

class Rectangle : public Intersectable {
	Triangle t1, t2;
public:
    Rectangle(vec4 r1, vec4 r2, vec4 r3, vec4 r4, Material *m)
            : Intersectable(m), t1(r1, r2, r3, m) , t2(r3, r4, r1, m) { }

    Hit intersect(Ray ray) {
        Hit hit;
        hit = t1.intersect(ray);
        if(hit.t > 0) return hit;
        hit = t2.intersect(ray);
        if(hit.t > 0) return hit;
        return hit;
    }

};

class Scene {
    Intersectable *objects[MAX_NUMBER_OF_OBJECTS];
    Light *lights[MAX_NUMBER_OF_LIGHT_SOURCES];
    Camera *camera;
    vec4 image[windowHeight * windowWidth];
    unsigned int nObjects; unsigned int nLights;
public:
    Scene(Camera *c) : nObjects(0), nLights(0), camera(c) {}

    void push_object(Intersectable *o) { objects[nObjects++] = o; }

    void push_light(Light *l) { lights[nLights++] = l; }

    Hit firstIntersect(Ray ray) {
        Hit bestHit;
        for(int i = 0; i < nObjects; i++) {
            Hit hit = objects[i]->intersect(ray);
            if(hit.t > 0 && (bestHit.t < 0 || hit.t < bestHit.t))
                bestHit = hit;
        }
        return bestHit;
    }

	vec4 trace(Ray& ray, unsigned int depth = 0) {
		vec4 La(0.2823f, 0.4627f, 1.0f);
		//vec4 La(0.0f, 0.0f, 0.0f);
		if(depth > MAX_NUMBER_OF_RECURSION_DEPTH) return La;
		Hit hit = firstIntersect(ray);
		if(hit.t < 0) return La;
		vec4 outRadiance = La * hit.material->getKa();
		if(hit.material->getSurface() == rough) {
			for(unsigned int i = 0; i < nLights; i++) {
				Light *l = lights[i];
				vec4 lightDir = (l->position - hit.position).normalize();
				vec4 viewDir = camera->getViewDir(hit.position);
				vec4 hitPosition = hit.position + hit.normal * E * sign(vec4::dot(hit.normal, viewDir));
				Ray shadowRay(hitPosition, lightDir);
				Hit shadowHit = firstIntersect(shadowRay);
				if((shadowHit.t < 0) || (shadowHit.t > (hitPosition - l->position).length())) {
					vec4 inRad = hit.material->shade(hit.normal, viewDir, lightDir, l->Lout);
					outRadiance = outRadiance + inRad;
				}
			}
		} else {
			if(hit.material->getSurface() == reflective) {
				vec4 reflectionDir = (hit.material->reflect(ray.dir, hit.normal)).normalize();
				vec4 hitPosition = hit.position + hit.normal * E * sign(vec4::dot(hit.normal, ray.dir));
				Ray reflectedRay(hitPosition, reflectionDir);
				outRadiance = outRadiance + trace(reflectedRay, ++depth) * hit.material->Fresnel(ray.dir, hit.normal);
			}
			if(hit.material->getSurface() == refractive) {
				vec4 refractionDir = hit.material->refract(ray.dir, hit.normal);
				vec4 hitPosition = hit.position - hit.normal * E * sign(vec4::dot(hit.normal, ray.dir));
				Ray refractedRay(hitPosition, refractionDir);
				outRadiance = outRadiance + trace(refractedRay, ++depth) *
							  (unit_color - hit.material->Fresnel(ray.dir, hit.normal));
			}
		}
		return outRadiance;
	}

	vec4* render() {
		for(int Y = 0; Y < windowHeight; Y++)
			for(int X = 0; X < windowWidth; X++) {
				Ray ray = camera->getRay(X, Y);
				image[Y * windowWidth + X] = trace(ray);
			}
		return image;
	}
};

// handle of the shader program
unsigned int shaderProgram;

class FullScreenTexturedQuad {
	unsigned int vao, textureId;	// vertex array object id and texture id
public:
	void Create(vec4 image[windowWidth * windowHeight]) {
		glGenVertexArrays(1, &vao);	// create 1 vertex array object
		glBindVertexArray(vao);		// make it active

		unsigned int vbo;		// vertex buffer objects
		glGenBuffers(1, &vbo);	// Generate 1 vertex buffer objects

		// vertex coordinates: vbo[0] -> Attrib Array 0 -> vertexPosition of the vertex shader
		glBindBuffer(GL_ARRAY_BUFFER, vbo); // make it active, it is an array
		static float vertexCoords[] = { -1, -1,   1, -1,  -1, 1, 
			                             1, -1,   1,  1,  -1, 1 };	// two triangles forming a quad
		glBufferData(GL_ARRAY_BUFFER, sizeof(vertexCoords), vertexCoords, GL_STATIC_DRAW);	   // copy to that part of the memory which is not modified 
		// Map Attribute Array 0 to the current bound vertex buffer (vbo[0])
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,	0, NULL);     // stride and offset: it is tightly packed

		// Create objects by setting up their vertex data on the GPU
		glGenTextures(1, &textureId);  				// id generation
		glBindTexture(GL_TEXTURE_2D, textureId);    // binding

		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, windowWidth, windowHeight, 0, GL_RGBA, GL_FLOAT, image); // To GPU
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); // sampling
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	}

	void Draw() {
		glBindVertexArray(vao);	// make the vao and its vbos active playing the role of the data source
		int location = glGetUniformLocation(shaderProgram, "textureUnit");
		if (location >= 0) {
			glUniform1i(location, 0);		// texture sampling unit is TEXTURE0
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, textureId);	// connect the texture to the sampler
		}
		glDrawArrays(GL_TRIANGLES, 0, 6);	// draw two triangles forming a quad
	}
};

// The virtual world: single quad
FullScreenTexturedQuad fullScreenTexturedQuad;

Material pool_mat(rough, 0, vec4(),
                  vec4(0.0, 0.0, 0.0),
                  vec4(0.678431f, 0.84705f, 0.9019607f),
                  vec4(0.678431f, 0.84705f, 0.9019607f), 60);

Material grass(rough, 0, vec4(),
                vec4(0.0f, 0.3f, 0.0f),
                vec4(0.1044f, 0.543f, 0.123f),
                vec4(0.2f, 0.6f, 0.3), 60.0f);
// Pool 50m x 10m x 3m
Rectangle p_front(vec4(0, -30, 0), vec4(100, -30, 0), vec4(100, 0, 0), vec4(0, 0, 0), &pool_mat);
Rectangle p_right(vec4(100, 0, 500), vec4(100, 0, 0), vec4(100, -30, 0), vec4(100, -30, 500), &pool_mat);
Rectangle p_left(vec4(0, -30, 0), vec4(0, -30, 500), vec4(0, 0, 500), vec4(0, 0, 0), &pool_mat);
Rectangle p_back(vec4(0, -30, 500), vec4(100, -30, 500), vec4(100, 0, 500), vec4(0, 0, 500), &pool_mat);
Rectangle p_bottom(vec4(0, -30, 0), vec4(100, -30, 0), vec4(100, -30, 500), vec4(0, -30, 500), &pool_mat);
// Grass
Rectangle g_front(vec4(-400, 0, -400), vec4(500, 0, -400), vec4(500, 0, 0), vec4(-400, 0, 0), &grass);
Rectangle g_back(vec4(-400, 0, 900), vec4(500, 0, 900), vec4(400, 0, 500), vec4(-400, 0, 500), &grass);
Rectangle g_right(vec4(-400, 0, 500), vec4(-400, 0, 0), vec4(0, 0, 0), vec4(0, 0, 500), &grass);
Rectangle g_left(vec4(100, 0, 500), vec4(100, 0, 0), vec4(400, 0, 0), vec4(400, 0, 500), &grass);
// Water
Material water(refractive, 1.3f, vec4(1, 1, 1), vec4(0, 0, 0),
			   vec4(0,0,0), vec4(0,0,0), 0.0f);
Material gold(reflective, 0.75, vec4(0.6282f, 0.5558f, 0.366), vec4(), vec4(), vec4(), 0.4f);
Plane w_palne(vec4(0.0f, -2.0f, 0.0f), vec4(0.0f, 1.0f, 0.001f, 0),  &water);
// Camera  + Scene + Light
vec4 eye(0.0f, 10.0f, -300.0f, 0.0f);
vec4 lookat(0.0f, 0.0f, 0.0f, 0.0f);
vec4 up = vec4(0.0f, (windowHeight / 2.0f), 0.0f, 0.0f) - lookat;
vec4 right = vec4(windowWidth / 2.0f, 0.0f, 0.0f, 0.0f) - lookat;
vec4 white(1.0f, 1.0f, 1.0f);
vec4 light_position(25.0f, 25.0f, -350.0f);
vec4 light_position2(0.0f, 150.0f, -350.0f);
Light light(light_position, white);
Light light2(light_position2, white);
Camera camera(lookat, eye, up, right);
Scene scene(&camera);

// room-ball
Plane p0(vec4(0, -90, 0), vec4(0, 1, 0.0001f, 0), &grass);
Plane p1(vec4(0, 150, 0), vec4(0, -1, 0.0001f, 0), &grass);
Plane p2(vec4(150, 0, 0), vec4(-1, 0, 0.0001f, 0), &grass);
Plane p3(vec4(-150, 0, 0), vec4(1, 0, 0.0001f, 0), &grass);
Plane p4(vec4(0, 0, 250), vec4(0.00001f, 0, -1, 0), &grass);
Plane p5(vec4(0, 0, -500), vec4(0.00001f, 0, 1, 0), &grass);
//Plane p6(vec4(0, 0, 150), vec4(0.00001f, 0, -1), &water);
vec4 pp0(0, 0, -200);
Sphere s1(pp0, 25, &water);
vec4 pp1(-20, -10, -150);
Sphere s2(pp1, 25, &pool_mat);
vec4 pp2(25, 20, -120);
Sphere s3(pp2, 25, &pool_mat);
Triangle t0(vec4(-25, -25, -20), vec4(25, -25, -20), vec4(0, 50, -20), &water);
// Initialization, create an OpenGL context
void onInitialization() {
	glViewport(0, 0, windowWidth, windowHeight);
/*	scene.push_object(&p_front);
    scene.push_object(&p_right);
    scene.push_object(&p_left);
    scene.push_object(&p_back);
    scene.push_object(&p_bottom);
    scene.push_object(&g_front);
    scene.push_object(&g_back);
    scene.push_object(&g_right);
    scene.push_object(&g_left);
    scene.push_object(&w_palne);*/
	scene.push_object(&p0);
	//scene.push_object(&p1);
	//scene.push_object(&p2);
	//scene.push_object(&p3);
	//scene.push_object(&p4);
	//scene.push_object(&p5);
	scene.push_object(&s1);
	scene.push_object(&s2);
	scene.push_object(&s3);
    //scene.push_light(&light);
	scene.push_light(&light2);

    static vec4 *background;
	background = scene.render();
	fullScreenTexturedQuad.Create( background );

	// Create vertex shader from string
	unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
	if (!vertexShader) {
		printf("Error in vertex shader creation\n");
		exit(1);
	}
	glShaderSource(vertexShader, 1, &vertexSource, NULL);
	glCompileShader(vertexShader);
	checkShader(vertexShader, "Vertex shader error");

	// Create fragment shader from string
	unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
	if (!fragmentShader) {
		printf("Error in fragment shader creation\n");
		exit(1);
	}
	glShaderSource(fragmentShader, 1, &fragmentSource, NULL);
	glCompileShader(fragmentShader);
	checkShader(fragmentShader, "Fragment shader error");

	// Attach shaders to a single program
	shaderProgram = glCreateProgram();
	if (!shaderProgram) {
		printf("Error in shader program creation\n");
		exit(1);
	}
	glAttachShader(shaderProgram, vertexShader);
	glAttachShader(shaderProgram, fragmentShader);

	// Connect Attrib Arrays to input variables of the vertex shader
	glBindAttribLocation(shaderProgram, 0, "vertexPosition"); // vertexPosition gets values from Attrib Array 0

	// Connect the fragmentColor to the frame buffer memory
	glBindFragDataLocation(shaderProgram, 0, "fragmentColor");	// fragmentColor goes to the frame buffer memory

	// program packaging
	glLinkProgram(shaderProgram);
	checkLinking(shaderProgram);
	// make this program run
	glUseProgram(shaderProgram);
}

void onExit() {
	glDeleteProgram(shaderProgram);
	printf("exit");
}

// Window has become invalid: Redraw
void onDisplay() {
	glClearColor(0, 0, 0, 0);							// background color 
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the screen
	fullScreenTexturedQuad.Draw();
	glutSwapBuffers();									// exchange the two buffers
}

// Key of ASCII code pressed
void onKeyboard(unsigned char key, int pX, int pY) {
	if (key == 'd') glutPostRedisplay();         // if d, invalidate display, i.e. redraw
}

// Key of ASCII code released
void onKeyboardUp(unsigned char key, int pX, int pY) {

}

// Mouse click event
void onMouse(int button, int state, int pX, int pY) {
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {  // GLUT_LEFT_BUTTON / GLUT_RIGHT_BUTTON and GLUT_DOWN / GLUT_UP
	}
}

// Move mouse with key pressed
void onMouseMotion(int pX, int pY) {
}

// Idle event indicating that some time elapsed: do animation here
void onIdle() {
	long time = glutGet(GLUT_ELAPSED_TIME); // elapsed time since the start of the program
}

// Idaig modosithatod...
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

int main(int argc, char * argv[]) {
	glutInit(&argc, argv);
#if !defined(__APPLE__)
	glutInitContextVersion(majorVersion, minorVersion);
#endif
	glutInitWindowSize(windowWidth, windowHeight);				// Application window is initially of resolution 600x600
	glutInitWindowPosition(100, 100);							// Relative location of the application window
#if defined(__APPLE__)
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH | GLUT_3_2_CORE_PROFILE);  // 8 bit R,G,B,A + double buffer + depth buffer
#else
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);
#endif
	glutCreateWindow(argv[0]);

#if !defined(__APPLE__)
	glewExperimental = true;	// magic
	glewInit();
#endif

	printf("GL Vendor    : %s\n", glGetString(GL_VENDOR));
	printf("GL Renderer  : %s\n", glGetString(GL_RENDERER));
	printf("GL Version (string)  : %s\n", glGetString(GL_VERSION));
	glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &minorVersion);
	printf("GL Version (integer) : %d.%d\n", majorVersion, minorVersion);
	printf("GLSL Version : %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

	onInitialization();

	glutDisplayFunc(onDisplay);                // Register event handlers
	glutMouseFunc(onMouse);
	glutIdleFunc(onIdle);
	glutKeyboardFunc(onKeyboard);
	glutKeyboardUpFunc(onKeyboardUp);
	glutMotionFunc(onMouseMotion);

	glutMainLoop();
	onExit();
	return 1;
}